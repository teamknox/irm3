# irm3

 irm3 can read Infraed Red (IR) signal from Remote-controller via irMagician and write and store with JSON file as well. irMagican is released from Omiya-Gilen LLC.


## Getting started

 1. Setup (plug to USB port) irMagician, then confirm UART device at your host-PC such as Win, Linux (So far, Macintosh can not handle irMagician in driver issue...).  
 2. Cloning and fix code from above confirmed UART port of your PC.
 3. Python3 environment required with pyserial installed


## Commands  
 Following commnads for handling,

* Help
 -h, --help:
 Show help messages.

* Capture
 -c, --capture:    
 Capture IR-Signal from Remote-controller  

* Play  
 -p, --play:  
 Confirmation Stored IR-Signal for reading signal correctly.  

* Save
 -s, --save:  
  Save IR-signal.  

* File
 -f FILE, --file FILE:  
 IR-signal to file (json)  

* Temparature
 -t, --temperature:  
 Measure ambient irMagicianT temperature in degrees Celsius

* Version
-v, --version:  
Show firmware version


## How to use  

* Capture IR-Signal:  
 1. ```python3 ./irmcli3.py -c```  
 2. Press your target remote-controller
 3. ```python3 ./irmcli3.py -p```: Confirm your desired action  

* Save Captured IR-Signal:  
 1. ```python3 ./irmcli3.py -s -f ./<FILE_NAME>.json```  
 irmcli3 returns following message
 ```
  Saving IR data to ./<FILE_NAME>.json ...
  Done !
 ```

* Play Saved IR-Signal:  
 1. ```python3 ./irmcli3.py -p -f ./<FILE_NAME>.json```  
 irmcli3 returns following message
 ```
  Playing IR with ./fanoff.json ...
  ... Done !
 ```

 ## Acknowledgement
  Special thanks to [netbuffalo](https://github.com/netbuffalo/irmcli) for original packaging from my messy-work.

 ## License  
 Released under the MIT License.
